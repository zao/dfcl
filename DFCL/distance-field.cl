//  (C) Copyright Lars Viklund 2011. Distributed under the Boost
//  Software License, Version 1.0. (See accompanying file
//  LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

__kernel void naive_distance_field
(
 // data of source texture
 // 0 is black
 __global uchar const * src,
 
 // data of target texture
 // signed char, positive is internal pixel
 __global uchar * dst,
 
 // width of target texture
 // in target pixel units
 uint const tw,
 
 // height of target texture
 // in target pixel units
 uint const th,
 
 // width of target pixel
 // in source pixel units
 uint const scw,
 
 // height of target pixel
 // in source pixel units
 uint const sch,
 
 // number of pixels to analyze
 // in source pixel units
 uint const radius)
{
	int r = radius;
	uint const tx = get_global_id(0);
	uint const ty = get_global_id(1);

	uint const sw = tw * scw;
	uint const sh = th * sch;

	// location of source pixel southeast of the center of the target pixel
	uint const smidx = tx * scw + scw / 2;
	uint const smidy = ty * sch + sch / 2;

	// index of target pixel in output array
	uint const tidx = ty * tw + tx;

	uchar const center_count =
		( (src[(smidy - 1) * sw + smidx - 1] > 0)
		+ (src[(smidy - 1) * sw + smidx - 0] > 0)
		+ (src[(smidy - 0) * sw + smidx - 1] > 0)
		+ (src[(smidy - 0) * sw + smidx - 0] > 0)
		);

	if (center_count == 2)
	{
		dst[tidx] = 128;
		return;
	}

	bool const target_inside = center_count >= 3;
	int const target_sign = target_inside ? 1 : -1;
	
	float min_dist;
	if (center_count == 1 || center_count == 3)
	{
		min_dist = 2.0;
	}

	if (center_count == 0 || center_count == 4)
	{
		min_dist = 9000001;//radius * radius * 2;

		int2 off;
		uint2 cur;
		for (off.y = -r; off.y < r; ++off.y)
		{
			int const cy = smidy + off.y;
			if (cy < 0 || cy >= sh)
				continue;
			cur.y = cy;

			for (off.x = -r; off.x < r; ++off.x)
			{
				int const cx = smidx + off.x;
				if (cx < 0 || cx >= sw)
					continue;
				cur.x = cx;

				bool source_inside = src[cur.y * sw + cur.x] > 0;
				if (source_inside != target_inside)
				{
					float2 adj_off = { off.x - 0.5, off.y - 0.5 };
					float distance = dot(adj_off, adj_off);
					min_dist = min(min_dist, distance);
				}
			}
		}
	}
	dst[tidx] = max(0, min(255, 128 + (int)sqrt(min_dist) * target_sign * 128 / r));
}