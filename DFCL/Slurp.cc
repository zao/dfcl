//  (C) Copyright Lars Viklund 2011. Distributed under the Boost
//  Software License, Version 1.0. (See accompanying file
//  LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include <fstream>
#include <string>

std::string slurp(std::string filename)
{
	std::ifstream is(filename.c_str(), std::ios::binary);
	if (!is)
		return "";

	is.seekg(0, std::ios::end);
	size_t cb = is.tellg();
	is.seekg(0, std::ios::beg);

	std::string ret(cb + 1, '\0');
	is.read(&ret[0], cb);
	return ret;
}