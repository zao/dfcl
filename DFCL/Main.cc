//  (C) Copyright Lars Viklund 2011. Distributed under the Boost
//  Software License, Version 1.0. (See accompanying file
//  LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#include <cassert>
#include <iostream>
#include <string>
#include <vector>

#include <CL/opencl.h>
#include <SOIL.h>

#if defined(_WIN32)
#if defined(_DEBUG)
#pragma comment(lib, "soild.lib")
#else
#pragma comment(lib, "soil.lib")
#endif
#pragma comment(lib, "opencl.lib")
#include <windows.h>
#endif

#define LOOP(Var, Limit) for (int Var = 0; Var < (int)Limit; ++Var)
#define LOOPDOWN(Var, Limit) for (int Var = (int)Limit - 1; Var >= 0; --Var)

void __stdcall on_cl_notify(char const * err_info, void const * private_info, size_t cb, void * user_data)
{

}

enum ExitReason
{
	ExitReasonPlatformError = 1,
	ExitReasonNoPlatforms,
	ExitReasonDeviceError,
	ExitReasonNoDevices,
	ExitReasonInvalidProgram,
	ExitReasonInvalidArguments
};

std::string slurp(std::string filename);

void usage(char * prog_name)
{
	std::cerr << "usage: " << prog_name << " source.png target.bmp target_width spread_radius" << std::endl;
}

std::vector<cl_platform_id> enumerate_platforms()
{	
	cl_int error_code = 0;
	cl_uint platform_count = 0;
	std::vector<cl_platform_id> platforms;

	error_code = clGetPlatformIDs(0, NULL, &platform_count);
	if (CL_SUCCESS != error_code || 0 == platform_count)
	{
		return platforms;
	}
	
	platforms.resize(platform_count);
	error_code = clGetPlatformIDs(platform_count, &platforms[0], &platform_count);

	return platforms;
}

std::vector<cl_device_id> enumerate_devices(cl_platform_id platform)
{
	cl_int error_code = 0;
	cl_uint device_count = 0;
	std::vector<cl_device_id> devices;

	error_code = clGetDeviceIDs(platform, CL_DEVICE_TYPE_DEFAULT, 0, NULL, &device_count);
	if (CL_SUCCESS != error_code || 0 == device_count)
	{
		return devices;
	}

	devices.resize(device_count);
	error_code = clGetDeviceIDs(platform, CL_DEVICE_TYPE_DEFAULT, device_count, &devices[0], NULL);

	return devices;
}

int main(int argc, char * * argv)
{
	if (argc != 5)
	{
		usage(*argv);
		return ExitReasonInvalidArguments;
	}

	// Parse command line arguments
	char * source_file = argv[1];
	char * target_file = argv[2];
	char * ends[] = { 0, 0 };
	unsigned target_width = strtoul(argv[3], &ends[0], 10);
	unsigned spread_radius = strtoul(argv[4], &ends[1], 10);
	if (*ends[0] || *ends[1])
	{
		usage(*argv);
		return ExitReasonInvalidArguments;
	}
	
	// Load source image and allocate source/target buffers
	int src_width = 0, src_height = 0, src_channels = 0;
	unsigned char * src_image = SOIL_load_image(source_file, &src_width, &src_height, &src_channels, SOIL_LOAD_L);
	if (!src_image)
	{
		std::cerr << "Source image " << source_file << " did not load correctly." << std::endl;
		return ExitReasonInvalidArguments;
	}
	std::vector<unsigned char> src_buffer(src_image, src_image + src_width * src_height);
	SOIL_free_image_data(src_image);

	std::vector<unsigned char> dst_buffer(target_width * target_width);


	// OpenCL setup section
	cl_int error_code = 0;

	std::vector<cl_platform_id> platforms = enumerate_platforms();
	if (platforms.empty())
	{
		std::cerr << "No OpenCL platforms found." << std::endl;
		return ExitReasonNoPlatforms;
	}

	std::vector<cl_device_id> devices = enumerate_devices(platforms[0]);
	if (devices.empty())
	{
		std::cerr << "No OpenCL devices found for primary platform." << std::endl;
		return ExitReasonNoDevices;
	}

	cl_context ctx = clCreateContext(NULL, devices.size(), &devices[0], &on_cl_notify, NULL, &error_code);
	if (CL_SUCCESS != error_code)
	{
		std::cerr << "CreateContext error: " << error_code << std::endl;
	}
	
	// Set up source and destination global buffers
	cl_mem src = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, src_buffer.size(), &src_buffer[0], &error_code);
	cl_mem dst = clCreateBuffer(ctx, CL_MEM_WRITE_ONLY, dst_buffer.size(), NULL, &error_code);

	// Load and attempt to compile OpenCL kernel
	std::string program_source = slurp("distance-field.cl");
	char const* source_text = program_source.c_str();
	cl_program program = clCreateProgramWithSource(ctx, 1, &source_text, NULL, &error_code);

	error_code = clBuildProgram(program, 0, NULL, "", NULL, NULL);
	bool build_failed = (CL_SUCCESS != error_code);

	{
		cl_device_id device_id = 0;
		error_code = clGetProgramInfo(program, CL_PROGRAM_DEVICES, sizeof(cl_device_id), &device_id, NULL);

		size_t log_size = 0;
		std::string log;
		error_code = clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		log.resize(log_size + 1);
		error_code = clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size, &log[0], NULL);

		std::cerr << "OpenCL compilation output:\n----\n" << log << "----" << std::endl;
#if defined(_WIN32)
		OutputDebugStringA(log.c_str());
		OutputDebugStringA("\n");
#endif
		if (build_failed)
			return ExitReasonInvalidProgram;
	}

	cl_kernel kernel = clCreateKernel(program, "naive_distance_field", &error_code);

	{
		/*
		__global uchar const * src,
		__global uchar * dst,
		uint const tw,
		uint const th,
		uint const scw,
		uint const sch,
		uint const radius)
		*/
		unsigned tw = target_width, th = tw, scw = src_width / tw, sch = src_height / th, radius = spread_radius;
		clSetKernelArg(kernel, 0, sizeof(cl_mem), &src);
		clSetKernelArg(kernel, 1, sizeof(cl_mem), &dst);
		clSetKernelArg(kernel, 2, sizeof(unsigned), &tw);
		clSetKernelArg(kernel, 3, sizeof(unsigned), &th);
		clSetKernelArg(kernel, 4, sizeof(unsigned), &scw);
		clSetKernelArg(kernel, 5, sizeof(unsigned), &sch);
		clSetKernelArg(kernel, 6, sizeof(unsigned), &radius);

		size_t global_ws[] = { tw, th };
		
		cl_command_queue queue = clCreateCommandQueue(ctx, devices[0], 0, &error_code);
		cl_event compute_event;
		error_code = clEnqueueNDRangeKernel(queue, kernel, 2, NULL, global_ws, NULL, 0, NULL, &compute_event);

		error_code = clEnqueueReadBuffer(queue, dst, CL_TRUE, 0, dst_buffer.size(), &dst_buffer[0], 1, &compute_event, NULL);
		if (CL_SUCCESS != error_code)
		{
			assert(error_code);
		}
		clReleaseEvent(compute_event);
		clReleaseCommandQueue(queue);
		SOIL_save_image(target_file, SOIL_SAVE_TYPE_BMP, tw, th, 1, &dst_buffer[0]);
	}

	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseMemObject(src);
	clReleaseMemObject(dst);
	clReleaseContext(ctx);	
}