//  (C) Copyright Lars Viklund 2011. Distributed under the Boost
//  Software License, Version 1.0. (See accompanying file
//  LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

__kernel void add_gpu(__constant int const * src, __global int * dst, int const num)
{
	int const idx = get_global_id(0);

	if (idx < num)
		dst[idx] = src[idx] * src[idx];
}